from app import AppFactory

app = AppFactory.intialize()

if __name__ == '__main__':
    app.run(debug=True, port=5055)
