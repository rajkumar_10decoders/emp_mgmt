import jwt, jwt.api_jwt
from werkzeug.security import check_password_hash
from app.model.employee_model import Employee
from config import Config
from app import AppFactory


#
# def sign_up():
#     data = request.get_json()
#     hashed_password = generate_password_hash(data['password'], method='sha256')
#     new_employee = Secrets(username=data['username'], password=hashed_password)
#     new_employee.save_me()
#
#
# def login(username, password):
#     user_check = User_confirmation.check_user_exists(username)
#     pass_check = check_password_hash(user_check.secrets[0].password, password)
#     Logger_config.logger.info(f"{username}")
#     if not user_check or pass_check is False:
#         return jsonify({"error": "user not found"}, 401)
#     employee = Secrets.query.filter_by(username=username).first()
#     if check_password_hash(employee.password, password):
#         token = jwt.encode({
#             'id': employee.id,
#             'exp': datetime.datetime.utcnow() + datetime.timedelta(minutes=120)
#         }, Config.SECRET_KEY)
#         return jsonify({'token': token.decode('UTF-8')})


class User_confirmation:
    @classmethod
    def check_user_exists(cls, username):
        employee = Employee.by_employee_name(username)
        if len(employee) == 0:
            return None
        else:
            return employee[0]

    @classmethod
    def password_Verification(cls, password, hashed_password):
        is_match = AppFactory.bcrypt.check_password_hash(password, hashed_password)
        if is_match:
            return True
        else:
            return False


class JWT_Provider:
    @classmethod
    def jwt(cls, user):
        token = jwt.encode({
            'user_id': user.pid,
            'username': user.employee_name,
            'role': user.role
        }, Config.SECRET_KEY)

        return token.decode('utf-8')
