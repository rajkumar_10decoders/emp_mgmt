c = {
    "first_name": "testo1",
    "last_name": "testl",
    "gender": "male",
    "blood_group": "B + ve",
    "date_of_birth": "1999-5-5",
    "age": "23",
    "martial_status": "unmarried",
    "role": "user",
    "jop_title": "test engineer",
    "contact_number": "2368526239",
    "date_of_join": "2022-5-5",
    "active_status": True,
    "address_list": {"current_address": "test_address", "permanent_address": "test"}

}
