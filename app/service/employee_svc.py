import logging

import requests
from sqlalchemy import orm

from app import AppFactory
from app.model.address_model import Address
from app.model.employee_model import Employee
from app.model.person_model import Person
from app.model.secrets_model import Secrets
from app.model import SQLAconfig as sqla


class CustomException(Exception):
    pass


class Emp_svc:

    @classmethod
    def build_payload(cls, emp_entities):
        address_list = []
        if len(emp_entities.address) != 0:
            for add in emp_entities.address:
                address_dict = {
                    "current_address": add.current_address,
                    "permanent_address": add.permanent_address
                }
                address_list.append(address_dict)
        else:
            print("Error in address list")

        per_ent = emp_entities.person[0] if len(emp_entities.person) else None

        emp_dict = {
            "Employee_name": emp_entities.employee_name,
            "Jop title": emp_entities.jop_title,
            "Gender": per_ent.gender,
            "Blood group": per_ent.blood_group,
            "Date of birth": per_ent.date_of_birth,
            "Age": per_ent.age,
            "Martial status": per_ent.martial_status,
            "Address List": address_list
        }
        return emp_dict

    @classmethod
    def add_emp(cls, role, jop_title, contact_number, date_of_join,
                active_status, first_name, last_name, gender, blood_group, address_list,
                date_of_birth, age, martial_status, password):
        try:
            emp_ent = Employee()
            # address
            for add in address_list:
                current_address = add['current_address']
                permanent_address = add['permanent_address']
                add_address = Address(current_address=current_address, permanent_address=permanent_address)
                emp_ent.address.append(add_address)
            # person
            per_ent = Person(first_name=first_name,
                             last_name=last_name,
                             gender=gender,
                             blood_group=blood_group,
                             date_of_birth=date_of_birth,
                             age=age,
                             martial_status=martial_status
                             )
            # secrets
            secret_ent = Secrets(username=first_name + last_name,
                                 password=AppFactory.bcrypt.generate_password_hash(password))

            emp_ent.employee_name = first_name + last_name
            emp_ent.role = role
            emp_ent.jop_title = jop_title
            emp_ent.contact_number = contact_number
            emp_ent.date_of_join = date_of_join
            emp_ent.active_status = active_status

            emp_ent.secrets.append(secret_ent)

            emp_ent.person.append(per_ent)

            emp_ent.save_me()
            logging.info(F'Log from insert_emp, Employee:  {emp_ent}')
            return emp_ent

        except Exception as e:
            raise e

    @classmethod
    def del_emp(cls, id):
        emp = Employee.by_id(id)
        if emp:
            emp.delete_me()
            return True
        else:
            return False

    @classmethod
    def update_emp(cls, emp_ent, data):
        try:
            msg = ''
            data_keys_list = data.keys()
            person_key = Person().__table__.columns.keys()
            person = Person.by_id(emp_ent.person[0].pid)
            for data_key in data_keys_list:
                if data_key in person_key:
                    msg = f'Updated {data_key} for employee id: {emp_ent.pid}'
                    person.__dict__[data_key] = data[data_key]

                else:
                    raise CustomException(F'Error in updating Employee details,ID : {emp_ent.pid}')
            person.save_me()
            return msg
        except Exception as e:
            raise e

    @classmethod
    def get_all(cls, limit=None, offset=None):
        employee_list = Employee.get_all(limit=limit, offset=offset)
        payload_list = []
        for emp in employee_list:
            employee_dict = cls.build_payload(emp)
            payload_list.append(employee_dict)
        return payload_list

    @classmethod
    def get_all_pagination(cls, limit=None, offset=None):

        query = sqla.session.query(Employee)
        query = query.options(orm.joinedload('person'))
        query = query.options(orm.joinedload('addresses'))
        query = query.limit(limit).offset(offset)
        query = query.all()
        return query

    @classmethod
    def person_update(cls, person: Person, data):
        try:
            person.first_name = data['first_name']
            person.last_name = data['last_name']
            person.gender = data['gender']
            person.date_of_birth = data['date_of_birth']
            person.age = data['age']
            person.martial_status = data['martial_status']
            person.blood_group = data['blood_group']
            person.save_me()
            return "Updated"
        except Exception as e:
            raise e
