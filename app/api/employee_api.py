import json

from flask import Blueprint, request, jsonify, make_response
from flask_restx import Resource

from app.model.employee_model import Employee
from app.service.employee_svc import Emp_svc
from app.service.login_svc import User_confirmation, JWT_Provider
from helper.jwt_auth import adminauth, userauth
from swagger_stack.swagger import SwaggerConfiguration

emp_api = Blueprint('emp_api', __name__)

swagger_route = SwaggerConfiguration.swagger_route


@emp_api.route('/healthcheck', methods=['GET'])
def health_check():
    return 'Success', 200


@swagger_route.route('/healthcheck')
class HelloWorld(Resource):
    def get(self):
        return make_response(jsonify({"message": "Helloworld"}, 200))


# @emp_api.route('/create', methods=['POST', 'GET'])
@swagger_route.route('/create')
@swagger_route.expect(SwaggerConfiguration.auth_token, SwaggerConfiguration.add_emp)
class create_employee(Resource):
    @adminauth
    def post(self, current_user):
        try:
            first_name = request.json.get('first_name')
            last_name = request.json.get('last_name')
            gender = request.json.get('gender')
            blood_group = request.json.get('blood_group')
            date_of_birth = request.json.get('date_of_birth')
            age = request.json.get('age')
            martial_status = request.json.get('martial_status')
            role = request.json.get('role')
            jop_title = request.json.get('jop_title')
            contact_number = request.json.get('contact_number')
            date_of_join = request.json.get('date_of_join')
            active_status = request.json.get('active_status')
            address_list = request.json.get('address_list')
            password = last_name + date_of_birth.replace("-", "")
            employee_details = Emp_svc.add_emp(first_name=first_name, last_name=last_name, gender=gender,
                                               blood_group=blood_group, date_of_birth=date_of_birth, age=age,
                                               martial_status=martial_status, role=role, jop_title=jop_title,
                                               contact_number=contact_number, date_of_join=date_of_join,
                                               active_status=active_status, address_list=address_list,
                                               password=password)
            return make_response(jsonify({"employee_id": employee_details.pid, "employee_name": first_name + last_name,
                                          "username": first_name + last_name,
                                          "password": password}), 200)
        except Exception as e:
            raise e


@swagger_route.route('/get_all')
@swagger_route.expect(SwaggerConfiguration.auth_token, SwaggerConfiguration.limit, SwaggerConfiguration.offset)
# @emp_api.route('/get_employee', methods=['GET'])
class Get_all(Resource):
    @adminauth
    def get(self, current_user):
        try:
            limit = request.args.get('limit')
            offset = request.args.get('offset')
            get_all_response = Emp_svc.get_all(limit, offset)
            return make_response(jsonify({"message": get_all_response}, 200))
        except Exception as e:
            return make_response(jsonify({"Error": e}, 500))


@swagger_route.route('/delete')
@swagger_route.expect(SwaggerConfiguration.auth_token, SwaggerConfiguration.del_emp)
# @emp_api.route('/delete_employee', methods=['DELETE'])
class DeleteEmployee(Resource):
    @adminauth
    def delete(self, current_user):
        try:
            employee_id = request.args.get('employee_id')
            emp_delete = Emp_svc.del_emp(id=employee_id)
            if not emp_delete:
                return make_response(jsonify({"message": "record not found or already deleted"}, 404))
            return jsonify({"!DELETED RECORD!": employee_id}, 204)
        except Exception as e:
            return make_response(jsonify({"Error": "Error in Record Deletion", "Error_type": e}, 500))


@swagger_route.route('/update')
@swagger_route.expect(SwaggerConfiguration.auth_token, SwaggerConfiguration.update)
class UpdateEmployee(Resource):
    @userauth
    def put(self, current_user):
        try:
            if not current_user:
                return make_response('Record not found')
            message = Emp_svc.person_update(current_user.person[0], request.json)
            return make_response({"Message": message}, 200)
        except Exception as e:
            return make_response({"Error": "Failed to update"}, 400)


@swagger_route.route('/login')
@swagger_route.expect(SwaggerConfiguration.login_parser)
# @emp_api.route('/login', methods=['POST'])
class Login(Resource):
    def post(self):
        try:
            username = request.form.get('username')
            password = request.form.get('password')
            user = User_confirmation.check_user_exists(username)
            pass_chk = User_confirmation.password_Verification(user.secrets[0].password,
                                                               password)
            if not user or pass_chk is False:
                return make_response('could not verify the login credentials', 401)
            token = JWT_Provider.jwt(user)
            return make_response(jsonify({'token': token}), 201)
        except Exception as e:
            return make_response(jsonify({"message": e.args[0]}), 500)


@swagger_route.route('/my_profile')
@swagger_route.expect(SwaggerConfiguration.auth_token)
# @emp_api.route('/my_profile', methods=['GET'])
class GetEmployeeID(Resource):
    @userauth
    def get(self, current_user):
        try:
            employee = Emp_svc.build_payload(current_user)
            return make_response(jsonify(employee))
        except Exception as e:
            return jsonify({"Error": e}, 500)
