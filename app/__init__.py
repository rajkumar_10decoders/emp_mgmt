from flask import Flask
from flask_bcrypt import Bcrypt
from flask_cors import CORS

from app.model import SQLAconfig
from config import Config
from helper.logging import Logger_config
from swagger_stack.swagger import SwaggerConfiguration


class AppFactory:
    bcrypt = None

    @classmethod
    def intialize(cls):
        try:
            app = Flask(__name__)
            CORS(app)
            cls.bcrypt = Bcrypt(app)
            config = Config()
            app.config['SECRET_KEY'] = config.SECRET_KEY

            # blueprint
            SwaggerConfiguration.intialize(app)
            from app.api.employee_api import emp_api
            app.register_blueprint(emp_api, url_prefix='/emp_mgmt/v1')

            # database
            SQLAconfig.intialize()

            # Mongodb

            # tracer
            cls.tracer = Logger_config.intialize()

            with app.app_context():
                pass
            return app
        except Exception as e:
            print(F"{e} Error occurred,Please check app factory")
