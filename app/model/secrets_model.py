import sqlalchemy
from sqlalchemy import Column, Integer, String

from app.model import SQLAconfig as sa
from app.model.base_model import BaseModel


class Secrets(BaseModel):
    __tablename__ = "Secrets"
    # login_credentials = Column("login_credentials", String(100))
    employee_id = Column("employee_id", Integer, sqlalchemy.ForeignKey('Employee.pid'), nullable=False)
    username = Column("username", String(50), nullable=False)
    password = Column("password", String(100), nullable=False)

    def __repr__(self):
        return f"Secrets {self.pid}"

    @classmethod
    def by_emp_id(cls, id):
        try:
            q = sa.session.query(cls)
            fil = q.filter(cls.employee_id == id)
            record = fil.scalar()
            return record
        except Exception as e:
            sa.session.rollback()
            raise f"by_id{e}"
