import sqlalchemy
from sqlalchemy import Column, String, Date, Integer

from app.model.base_model import BaseModel


class Person(BaseModel):
    __tablename__ = "Person"
    employee_id = Column("employee_id", Integer, sqlalchemy.ForeignKey('Employee.pid'), nullable=False)
    first_name = Column("first_name", String(30), nullable=False)
    last_name = Column("last_name", String(30), nullable=False)
    gender = Column("gender", String(10), nullable=False)
    blood_group = Column("blood_group", String(5), nullable=False)
    date_of_birth = Column("date_of_birth", Date, nullable=False)
    age = Column("age", Integer, nullable=False)
    martial_status = Column("martial_status", String(10))

    def __repr__(self):
        return f"Person {self.pid}"

    @classmethod
    def insert_person(cls, first_name,
                      last_name,
                      gender,
                      blood_group,
                      date_of_birth,
                      age,
                      martial_status):
        try:
            pers_ent = Person()
            pers_ent.first_name = first_name
            pers_ent.last_name = last_name
            pers_ent.gender = gender
            pers_ent.blood_group = blood_group
            pers_ent.date_of_birth = date_of_birth
            pers_ent.age = age
            pers_ent.martial_status = martial_status
            pers_ent.save_me()
            return pers_ent
        except Exception as e:
            raise e

    @classmethod
    def person_delete(cls, id):
        person = Person.by_id(id)
        if person:
            person.delete_me()
            return True
        else:
            return False
