import pymysql
from sqlalchemy import create_engine, MetaData
from sqlalchemy.orm import sessionmaker

pymysql.install_as_MySQLdb()
from config import Config


class SQLAconfig:
    session = None
    engine = None

    @classmethod
    def intialize(cls):
        SQLALCHEMY_DB_URI = F"mysql+pymysql://{Config.DB_USER}:{Config.DB_PASS}@{Config.DB_HOST}:3306/{Config.DB_NAME}"

        cls.engine = create_engine(SQLALCHEMY_DB_URI)
        conn = cls.engine.connect()
        Session = sessionmaker(bind=cls.engine, autoflush=False, autocommit=False)
        cls.session = Session()
        metadata = MetaData()
        # need to import models here
        from app.model.base_model import base
        from app.model.employee_model import Employee
        from app.model.address_model import Address
        from app.model.person_model import Person
        from app.model.secrets_model import Secrets
        base.metadata.create_all(bind=cls.engine)
