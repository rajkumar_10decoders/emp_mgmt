import sqlalchemy
from sqlalchemy import Column, String, Integer

from app.model.base_model import BaseModel


class Address(BaseModel):
    __tablename__ = "Address"
    employee_id = Column("employee_id", Integer, sqlalchemy.ForeignKey('Employee.pid'), nullable=False)
    current_address = Column("current_address", String(100), nullable=False)
    permanent_address = Column("permanent_address", String(100), nullable=False)

    def __repr__(self):
        return F"Address {self.pid}"

    # @classmethod
    # def insert_address(cls, current_address, permanent_address):
    #     try:
    #         addr_ent = Address()
    #         addr_ent.current_address = current_address
    #         addr_ent.permanent_address = permanent_address
    #         addr_ent.save_me()
    #         return addr_ent
    #     except Exception as e:
    #         raise f"insert_address , {e}"
    #
    # @classmethod
    # def delete_address(cls, id):
    #     address = Address.by_id(id)
    #     if address:
    #         address.delete_me()
    #         return True
    #     else:
    #         return False

