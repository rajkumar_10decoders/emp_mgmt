# employee model

from sqlalchemy import Column, String, Date, Boolean
from sqlalchemy.orm import relationship

from app.model.base_model import BaseModel


class Employee(BaseModel):
    __tablename__ = "Employee"
    employee_name = Column("employee_name", String(100), nullable=False)
    role = Column("role", String(20), nullable=False)
    jop_title = Column("job_title", String(20), nullable=False)
    contact_number = Column("contact_number", String(20), nullable=False)
    date_of_join = Column("date_of_join", Date, nullable=False)
    active_status = Column("active_status", Boolean)
    # relationship
    address = relationship('Address', backref='Employee', cascade="all,delete", lazy='joined')
    person = relationship('Person', backref='Employee', cascade="all,delete", lazy='joined')

    # secrets
    secrets = relationship('Secrets', backref='Employee', cascade="all,delete", lazy='joined')

    # Using backref just automates the creation of a relationship property at the other end.
    # cascade ,if the child data is either deleted or updated when the parent data is deleted or updated.
    # https://medium.com/@ns2586/sqlalchemys-relationship-and-lazy-parameter-4a553257d9ef
    # https://engineering.shopspring.com/speed-up-with-eager-loading-in-sqlalchemy-87a176cfd7ad
    def __repr__(self):
        # returns string representation
        return '<Employee %r>' % self.pid

    def del_emp(self=None):
        emp = Employee.by_id(self)
        if emp:
            emp.delete_me()
            return True
        else:
            return False

    def update_emp(self=None):
        pass

    @classmethod
    def by_employee_name(cls, employee_name):
        employee = cls.by_key_val({'employee_name': employee_name})
        return employee
