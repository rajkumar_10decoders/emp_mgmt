import datetime
import sqlalchemy
from sqlalchemy import Column, Integer
from sqlalchemy.ext.declarative import declarative_base

from app.model import SQLAconfig as sa

base = declarative_base()


class BaseModel(base):
    __abstract__ = True
    pid = Column('pid', Integer, primary_key=True, nullable=False)
    created_on = Column('created_on', sqlalchemy.DATETIME, default=datetime.datetime.now())
    updated_on = Column('updated_on', sqlalchemy.DATETIME, default=datetime.datetime.now())

    def save_me(self):
        # save the record
        try:
            sa.session.add(self)
            sa.session.commit()
        except Exception as e:
            sa.session.rollback()
            raise e

    def delete_me(self):
        # delete the record
        try:
            sa.session.delete(self)
            sa.session.commit()
        except Exception as e:
            sa.session.rollback()
            raise e

    @classmethod
    def by_id(cls, id):
        try:
            q = sa.session.query(cls)
            fil = q.filter(cls.pid == id)
            record = fil.scalar()
            return record
        except Exception as e:
            sa.session.rollback()
            raise f"by_id{e}"

    @classmethod
    def get_all(cls, limit=None, offset=None):
        # returns list
        try:
            query = sa.session.query(cls)
            if limit and offset:
                query = query.limit(limit).offset(offset)
            query = query.all()
            return query
        except Exception as e:
            sa.session.rollback()
            raise f"get_all {e} "

    @classmethod
    def by_ids(cls, ids):
        try:
            query = sa.session.query(cls)
            entities = query.filter(cls.pid.in_(ids)).all()
            return entities
        except Exception as e:
            sa.session.rollback()
            raise f"by_ids {e}"

    @classmethod
    def by_key_val(cls, key_val):
        try:
            query = sa.session.query(cls)
            for k, v in key_val.items():
                attributes = getattr(cls, k)
                query = query.filter(attributes == v)
            entities = query.all()
            return entities
        except Exception as e:
            sa.session.rollback()
            raise e

    def get_attrib_list(self):
        # noinspection PyUnresolvedReferences
        column_names = [column.name for column in self.__table__.columns]
        return column_names
