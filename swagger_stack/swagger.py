from flask import Blueprint, Flask
from flask_restx import Api, fields


class SwaggerConfiguration:
    limit = None
    offset = None
    auth_token = None
    login_parser = None
    del_emp = None
    swagger_route = None
    get_all_emp = None
    add_emp = None
    update = None

    @classmethod
    def intialize(cls, app):
        blueprint = Blueprint('api', __name__, url_prefix='/v1')
        api = Api(blueprint)
        app.register_blueprint(blueprint)
        cls.swagger_route = api.namespace('employee_management', description="employee_management swagger")
        var = api.parser()
        cls.add_employee(api)
        cls.delete_emp(api)
        cls.limit = api.parser().add_argument('limit', type=int, location='body', required=False)
        cls.offset = api.parser().add_argument('offset', type=int, location='body', required=False)
        cls.token_input(api)
        cls.swagger_inputs(api)
        cls.update_emp(api)

    @classmethod
    def add_employee(cls, api):
        address_fields = api.model('address',
                                   {'current_address': fields.String,
                                    'permanent_address': fields.String})
        cls.add_emp = api.model('Resource',
                                {
                                    "first_name": fields.String,
                                    "last_name": fields.String,
                                    "gender": fields.String,
                                    "blood_group": fields.String,
                                    "date_of_birth": fields.Date,
                                    "age": fields.Integer,
                                    "martial_status": fields.String,
                                    "role": fields.String,
                                    "jop_title": fields.String,
                                    "contact_number": fields.String,
                                    "date_of_join": fields.Date,
                                    "active_status": fields.Boolean,
                                    "address_list": fields.List(fields.Nested(address_fields))
                                }
                                )

    @classmethod
    def delete_emp(cls, api):
        cls.del_emp = api.parser()
        cls.del_emp.add_argument('employee_id', type=int, location='body', required=True, help="employee_id")

    @classmethod
    def swagger_inputs(cls, api):
        cls.login_parser = api.parser()
        username = cls.login_parser.add_argument('username', type=str, location='form', help='', required=True)
        password = cls.login_parser.add_argument('password', type=str, location='form', help='password', required=True)

    @classmethod
    def token_input(cls, api):
        cls.auth_token = api.parser()
        token = cls.auth_token.add_argument('token', type=str, location='headers', help='token', required=True)

    @classmethod
    def update_emp(cls, api):
        cls.update = api.model('Resource_update', {"first_name": fields.String,
                                                   "last_name": fields.String,
                                                   "gender": fields.String,
                                                   "blood_group": fields.String,
                                                   "date_of_birth": fields.Date,
                                                   "age": fields.Integer,
                                                   "martial_status": fields.String, })
