from functools import wraps

import jwt
from flask import request, jsonify, make_response

from app.model.employee_model import Employee
from config import Config


# import secrets
# secrets.token_hex(16)
# '8d252aadf54f196727242bbb7003e80c'

def userauth(func):
    @wraps(func)
    def decorated(*args, **kwargs):
        token = None
        if 'Token' in request.headers:
            token = request.headers['Token']
        if not token:
            return make_response(
                jsonify({'message': 'Token is missing !!'}), 401)

        try:
            data = jwt.decode(token, Config.SECRET_KEY)
            current_user = Employee.by_id(data['user_id'])
        except Exception as e:

            return make_response('invalid token', 401)
        kwargs['current_user'] = current_user
        return func(*args, **kwargs)

    return decorated


def adminauth(func):
    @wraps(func)
    def decorated(*args, **kwargs):
        token = None
        if 'Token' in request.headers:
            token = request.headers['Token']
        if not token:
            return make_response(jsonify({'message': 'Token is missing !!'}), 401)

        try:
            data = jwt.decode(token, Config.SECRET_KEY)
            current_user = Employee.by_id(data['user_id'])
            if current_user.role not in ('admin', 'ADMIN'):
                return make_response('unauthorized to perform operation', 403)
        except Exception as e:
            return make_response('invalid token', 401)
        return func(current_user, *args, **kwargs)

    return decorated
