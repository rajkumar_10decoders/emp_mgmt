import logging
import time

from jaeger_client import Config


class Logger_config:

    logger = None

    @classmethod
    def intialize(cls):
        cls.logger = logging.getLogger()
        cls.logger.setLevel(logging.DEBUG)
        cls.logger.info("logger started ")

        logging.basicConfig(format='%(asctime)s %(message)s', filemode='w')

        config = Config(
            config={  # usually read from some yaml config
                'sampler': {
                    'type': 'const',
                    'param': 1,
                },
                'logging': True,
            },
            service_name='Employee_Assessment',
            validate=True,
        )
        # this call also sets opentracing.tracer
        tracer = config.initialize_tracer()

        return tracer

        # with tracer.start_span('TestSpan') as span:
        #     span.log_kv({'event': 'test message', 'life': 42})
        #
        #     with tracer.start_span('ChildSpan', child_of=span) as child_span:
        #         child_span.log_kv({'event': 'down below'})
        #
        # time.sleep(2)
        # tracer.close()
